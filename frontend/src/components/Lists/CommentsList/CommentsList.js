import React from 'react';
import {Grid} from "@material-ui/core";
import CommentItem from "./CommentItem/CommentItem";

const CommentsList = ({comments}) => {
    return (
        <Grid container spacing={2}>
            {comments ? comments.map(item => (
                <CommentItem
                    key={item._id}
                    user={item.user_id.username}
                    datetime={item.datetime}
                    text={item.text}
                />
            )) : <h4>No comments here</h4>}
        </Grid>
    );
};

export default CommentsList;