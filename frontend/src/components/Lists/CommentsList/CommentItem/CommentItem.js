import React from 'react';
import {Card, CardContent, CardHeader, Grid} from "@material-ui/core";

const CommentItem = ({text, user, datetime}) => {
    return (
        <Grid item xs={12} md={12} lg={12}>
            <Card>
                <CardHeader  title={`${datetime} by ${user}`}/>
                <CardContent>
                    <p>{text}</p>
                </CardContent>
            </Card>
        </Grid>
    );
};

export default CommentItem;