import React from 'react';
import {
    Card,
    CardActions,
    CardContent,
    CardHeader,
    CardMedia,
    Grid,
    IconButton,
    makeStyles,
    Typography
} from "@material-ui/core";
import {apiUrl} from "../../../../config";
import {Link} from "react-router-dom";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";

const useStyles = makeStyles({
    card: {
        width: "100%"
    },
    media: {
        height: 0,
        paddingTop: '56.25%'
    }
})

const PostItem = ({datetime, username, image, title, id}) => {
    const classes = useStyles();

    let cardImage = "https://cdn-a.william-reed.com/var/wrbm_gb_food_pharma/storage/images/9/2/8/5/235829-6-eng-GB/Feed-Test-SIC-Feed-20142_news_large.jpg";

    if (image) {
        cardImage = apiUrl + '/' + image;
    }

    return (
        <Grid item>
            <Card className={classes.card}>
                <CardHeader title={`${datetime} by ${username}`}/>
                <CardMedia
                    image={cardImage}
                    className={classes.media}
                />
                <CardContent>
                    <Typography variant="h4">
                        Title: {title}
                    </Typography>
                </CardContent>
                <CardActions>
                    <IconButton component={Link} to={'/posts/' + id}>
                        <ArrowForwardIcon/>
                    </IconButton>
                </CardActions>
            </Card>

        </Grid>
    );
};

export default PostItem;