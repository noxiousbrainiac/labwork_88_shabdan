import React from 'react';
import PostItem from "./PostItem/PostItem";
import {Grid} from "@material-ui/core";

const PostsList = ({posts}) => {
    return (
        <Grid container spacing={2}>
            {posts
                ? posts.map(item => (
                    <PostItem
                        key={item._id}
                        datetime={item.datetime}
                        username={item.user_id.username}
                        image={item.image}
                        title={item.title}
                        id={item._id}
                    />
            )) : <h2>No posts here!</h2>}
        </Grid>
    );
};

export default PostsList;