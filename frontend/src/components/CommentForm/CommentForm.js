import React, {useState} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import {postComment} from "../../store/actions/commentsActions";
import {useDispatch} from "react-redux";

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2)
    },
}));

const CommentForm = ({post_id}) => {
    const classes = useStyles();
    const dispatch = useDispatch();

    const [state, setState] = useState({
        text: "",
        post_id: post_id
    });

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const onClick = () => {
        dispatch(postComment(state));
    }

    return (
        <Grid
            container
            direction="column"
            spacing={2}
            className={classes.root}
            autoComplete="off"
        >
            <Grid item xs>
                <TextField
                    fullWidth
                    variant="outlined"
                    label="Text"
                    name="text"
                    value={state.text}
                    onChange={inputChangeHandler}
                />
            </Grid>

            <Grid item xs>
                <Button onClick={onClick} type="button" color="primary" variant="contained">Add</Button>
            </Grid>
        </Grid>
    );
};

export default CommentForm;