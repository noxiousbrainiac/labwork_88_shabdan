import React from 'react';
import {Button, makeStyles} from "@material-ui/core";
import {Link} from "react-router-dom";

const useStyles = makeStyles({
    btn: {
        color: "beige",
    }
})

const Anonymous = () => {
    const classes = useStyles();

    return (
        <>
            <Button
                className={classes.btn}
                component={Link}
                to={"/register"}
                color="inherit"
            >
                Sing up
            </Button>
            <Button
                className={classes.btn}
                component={Link}
                to={"/login"}
                color="inherit"
            >
                Sing in
            </Button>
        </>
    );
};

export default Anonymous;