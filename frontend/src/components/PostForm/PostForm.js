import React, {useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import {useSelector} from "react-redux";
import {Alert} from "@material-ui/lab";

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2)
    },
    alert: {
        marginTop: theme.spacing(3),
        width: "100%"
    }
}));

const PostForm = ({onSubmit}) => {
    const classes = useStyles();
    const error = useSelector(state => state.posts.errorCreatePost);

    const [input, setInput] = useState({
        title: "",
        description: "",
        image: null,
    });

    const submitFormHandler = e => {
        e.preventDefault();

        const formData = new FormData();
        Object.keys(input).forEach(key => {
            formData.append(key, input[key]);
        });

        onSubmit(formData);
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setInput(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setInput(prevState => {
            return {...prevState, [name]: file};
        });
    };

    return (
        <Grid
            container
            direction="column"
            spacing={2}
            component="form"
            className={classes.root}
            autoComplete="off"
            onSubmit={submitFormHandler}
        >
            {
                error &&
                <Alert severity="error" className={classes.alert}>
                    {error.message}
                </Alert>
            }

            <Grid item xs>
                <TextField
                    fullWidth
                    variant="outlined"
                    label="Title"
                    name="title"
                    value={input.title}
                    onChange={inputChangeHandler}
                />
            </Grid>

            <Grid item xs>
                <TextField
                    fullWidth
                    multiline
                    rows={3}
                    variant="outlined"
                    label="Description"
                    name="description"
                    value={input.description}
                    onChange={inputChangeHandler}
                />
            </Grid>

            <Grid item xs>
                <TextField
                    type="file"
                    name="image"
                    onChange={fileChangeHandler}
                />
            </Grid>

            <Grid item xs>
                <Button type="submit" color="primary" variant="contained">Create</Button>
            </Grid>

        </Grid>
    );
};

export default PostForm;