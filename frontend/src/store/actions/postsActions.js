import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import WarningIcon from '@material-ui/icons/Warning';
import {historyPush} from "./historyActions";

export const GET_POSTS_REQUEST = "GET_POSTS_REQUEST";
export const GET_POSTS_SUCCESS = "GET_POSTS_SUCCESS";
export const GET_POSTS_FAILURE = "GET_POSTS_FAILURE";

export const CREATE_POST_REQUEST = "CREATE_POST_REQUEST";
export const CREATE_POST_SUCCESS = "CREATE_POST_SUCCESS";
export const CREATE_POST_FAILURE = "CREATE_POST_FAILURE";

export const FETCH_POST_REQUEST = "FETCH_POST_REQUEST";
export const FETCH_POST_SUCCESS = "FETCH_POST_SUCCESS";
export const FETCH_POST_FAILURE = "FETCH_POST_FAILURE";

export const getPostsRequest = () => ({type: GET_POSTS_REQUEST});
export const getPostsSuccess = (posts) => ({type: GET_POSTS_SUCCESS, payload: posts});
export const getPostsFailure = (error) => ({type: GET_POSTS_FAILURE, payload: error});

export const createPostRequest = () => ({type: CREATE_POST_REQUEST});
export const createPostSuccess = () => ({type: CREATE_POST_SUCCESS});
export const createPostFailure = (error) => ({type: CREATE_POST_FAILURE, payload: error});

export const fetchPostRequest = () => ({type: FETCH_POST_REQUEST});
export const fetchPostSuccess = (postData) => ({type: FETCH_POST_SUCCESS, payload: postData});
export const fetchPostFailure = (error) => ({type: FETCH_POST_FAILURE, payload: error});

export const getPosts = () => async (dispatch) => {
    try {
        dispatch(getPostsRequest());
        const {data} = await axiosApi.get('/posts');
        dispatch(getPostsSuccess(data));
    } catch (e) {
        dispatch(getPostsFailure(e));
        if (e.response.status === 401) {
            toast.warning('You need login!');
        } else {
            toast.error('Could not fetch posts!', {
                theme: 'colored',
                icon: <WarningIcon/>
            });
        }
    }
}

export const createPost = (postData) => async (dispatch) => {
    try {
        dispatch(createPostRequest());
        await axiosApi.post('/posts', postData);
        dispatch(historyPush('/'));
        dispatch(createPostSuccess());
    } catch (e) {
        dispatch(createPostFailure(e));
        if (e.response.status === 401) {
            toast.warning('You need login!');
        } else {
            toast.error(e.response.data.error, {
                theme: 'colored',
                icon: <WarningIcon/>
            });
        }
    }
}

export const fetchPost = (id) => async (dispatch) => {
    try {
        dispatch(fetchPostRequest());
        const {data} = await axiosApi.get(`/posts/${id}`);
        dispatch(fetchPostSuccess(data));
    } catch (e) {
        dispatch(fetchPostFailure(e));
        if (e.response.status === 404) {
            toast.warning('Not found any info about Post');
        } else {
            toast.error('Something wrong!', {
                theme: 'colored',
                icon: <WarningIcon/>
            });
        }
    }
}