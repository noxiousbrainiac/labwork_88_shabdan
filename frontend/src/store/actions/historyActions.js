import history from "../../history";

export const historyPush = payload => () => {
    history.push(payload);
}

export const historyReplace = payload => () => {
    history.replace(payload);
}