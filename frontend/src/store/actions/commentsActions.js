import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import WarningIcon from "@material-ui/icons/Warning";

export const POST_COMMENT_REQUEST = "POST_COMMENT_REQUEST";
export const POST_COMMENT_SUCCESS = "POST_COMMENT_SUCCESS";
export const POST_COMMENT_FAILURE = "POST_COMMENT_FAILURE";

export const FETCH_COMMENTS_REQUEST = "FETCH_COMMENTS_REQUEST";
export const FETCH_COMMENTS_SUCCESS = "FETCH_COMMENTS_SUCCESS";
export const FETCH_COMMENTS_FAILURE = "FETCH_COMMENTS_FAILURE";

export const postCommentRequest = () => ({type: POST_COMMENT_REQUEST});
export const postCommentSuccess = () => ({type: POST_COMMENT_SUCCESS});
export const postCommentFailure = (error) => ({type: POST_COMMENT_FAILURE, payload: error});

export const fetchCommentsRequest = () => ({type: FETCH_COMMENTS_REQUEST});
export const fetchCommentsSuccess = (comments) => ({type: FETCH_COMMENTS_SUCCESS, payload: comments});
export const fetchCommentsFailure = (error) => ({type: FETCH_COMMENTS_FAILURE, payload: error});

export const postComment = (comment) => async (dispatch) => {
    try {
        dispatch(postCommentRequest());
        await axiosApi.post(`/comments`, comment);
        dispatch(postCommentSuccess());
    } catch (e) {
        dispatch(postCommentFailure(e));
        if (e.response.status === 401) {
            toast.warning('You need login!');
        } else {
            toast.error(e.response.data.error, {
                theme: 'colored',
                icon: <WarningIcon/>
            });
        }
    }
}

export const fetchComments = (id) => async (dispatch) => {
    try {
        dispatch(fetchCommentsRequest());
        const {data} = await axiosApi.get(`/comments?post=${id}`);
        dispatch(fetchCommentsSuccess(data));
    } catch (e) {
        dispatch(fetchCommentsFailure(e));
        if (e.response.status === 404) {
            toast.warning('Not found any comments');
        } else {
            toast.error('Something wrong!', {
                theme: 'colored',
                icon: <WarningIcon/>
            });
        }
    }
}