import {
    CREATE_POST_FAILURE,
    CREATE_POST_REQUEST,
    CREATE_POST_SUCCESS, FETCH_POST_FAILURE, FETCH_POST_REQUEST, FETCH_POST_SUCCESS,
    GET_POSTS_FAILURE,
    GET_POSTS_REQUEST,
    GET_POSTS_SUCCESS
} from "../actions/postsActions";

const initialState = {
    posts: [],
    loading: false,
    errorPosts: null,
    errorCreatePost: null,
    post: null,
    errorPost: null
};

const postsReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_POSTS_REQUEST:
            return {...state, loading: true};
        case GET_POSTS_SUCCESS:
            return {...state, loading: false, posts: action.payload};
        case GET_POSTS_FAILURE:
            return {...state, loading: false, errorPosts: action.payload};
        case CREATE_POST_REQUEST:
            return {...state, loading: true};
        case CREATE_POST_SUCCESS:
            return {...state, loading: false};
        case CREATE_POST_FAILURE:
            return {...state, errorCreatePost: action.payload, loading: false};
        case FETCH_POST_REQUEST:
            return {...state, loading: true};
        case FETCH_POST_SUCCESS:
            return {...state, loading: false, post: action.payload};
        case FETCH_POST_FAILURE:
            return {...state, loading: false, errorPost: action.payload};
        default: return state;
    }
};

export default postsReducer;