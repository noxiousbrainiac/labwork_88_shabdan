import {
    FETCH_COMMENTS_FAILURE,
    FETCH_COMMENTS_REQUEST, FETCH_COMMENTS_SUCCESS,
    POST_COMMENT_FAILURE,
    POST_COMMENT_REQUEST,
    POST_COMMENT_SUCCESS
} from "../actions/commentsActions";

const initialState = {
    loading: false,
    error: null,
    comments: [],
    getError: null
};

const commentsReducer = (state = initialState, action) => {
    switch (action.type) {
        case POST_COMMENT_REQUEST:
            return {...state, loading: true};
        case POST_COMMENT_SUCCESS:
            return {...state, loading: false};
        case POST_COMMENT_FAILURE:
            return {...state, loading: false, error: action.payload};
        case FETCH_COMMENTS_REQUEST:
            return {...state, loading: true};
        case FETCH_COMMENTS_SUCCESS:
            return {...state, loading: false, comments: action.payload};
        case FETCH_COMMENTS_FAILURE:
            return {...state, loading: false, getError: action.payload};
        default: return state;
    }
};

export default commentsReducer;