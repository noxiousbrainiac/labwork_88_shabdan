import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getPosts} from "../../store/actions/postsActions";
import PostsList from "../../components/Lists/PostsList/PostsList";
import {CircularProgress} from "@material-ui/core";

const HomePage = () => {
    const dispatch = useDispatch();
    const posts = useSelector(state => state.posts.posts);
    const user = useSelector(state => state.users.user);
    const loading = useSelector(state => state.posts.loading);

    useEffect(() => {
        dispatch(getPosts());
    },[dispatch, user]);

    let renderPosts = (
        <>
            <h1>Posts</h1>
            <PostsList posts={posts}/>
        </>
    );

    if (loading === true) {
        renderPosts = (
            <div
                style={{
                    position: "relative",
                    paddingLeft: "50%",
                    paddingTop: "30%"
                }}
            >
                <CircularProgress
                    color="secondary"
                    size={100}
                />
            </div>
        );
    }

    return renderPosts;
};

export default HomePage;