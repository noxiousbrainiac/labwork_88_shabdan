import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Typography} from "@material-ui/core";
import PostForm from "../../components/PostForm/PostForm";
import {createPost} from "../../store/actions/postsActions";

const NewPost = () => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);

    const onSubmit = async postData => {
        await dispatch(createPost(postData));
    };

    let form = (
        <>
            <Typography variant="h4">New post</Typography>
            <PostForm onSubmit={onSubmit}/>
        </>
    )

    if (!user) form = <h1>You have to log in before create post</h1>

    return form;
};

export default NewPost;