import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchPost} from "../../store/actions/postsActions";
import {apiUrl} from "../../config";
import CommentsList from "../../components/Lists/CommentsList/CommentsList";
import {CircularProgress, makeStyles} from "@material-ui/core";
import CommentForm from "../../components/CommentForm/CommentForm";
import {fetchComments} from "../../store/actions/commentsActions";
import {Alert} from "@material-ui/lab";

const useStyles = makeStyles(theme => ({
    alert: {
        marginTop: theme.spacing(3),
        width: "100%"
    }
}))

const PostPage = ({match}) => {
    const dispatch = useDispatch();
    const classes = useStyles();
    const post = useSelector(state => state.posts.post);
    const user = useSelector(state => state.users.user);
    const loading = useSelector(state => state.posts.loading);
    const comments = useSelector(state => state.comments.comments);
    const error = useSelector(state => state.comments.error);

    let image = "https://cdn-a.william-reed.com/var/wrbm_gb_food_pharma/storage/images/9/2/8/5/235829-6-eng-GB/Feed-Test-SIC-Feed-20142_news_large.jpg";

    if (post?.image) {
        image = apiUrl + '/' + post?.image;
    }

    useEffect(() => {
        dispatch(fetchPost(match.params.id));
        const interval = setInterval(() => {
            dispatch(fetchComments(match.params.id));
        }, 2000);
        return () => clearInterval(interval);
    }, [dispatch, match.params.id])

    let postContainer = (
        <>
            <img src={image} alt="Post" style={{width: "300px", height: "100%", margin: "20px"}}/>
            <h1>Title: {post?.title}</h1>
            <p>Description: {post?.description}</p>
            {
                error &&
                <Alert severity="error" className={classes.alert}>
                    {error.message}
                </Alert>
            }
            {user ? <CommentForm post_id={match.params.id}/> : null}
            <CommentsList comments={comments}/>
        </>
    )

    if (loading === true) {
        postContainer = (
            <div
                style={{
                    position: "relative",
                    paddingLeft: "50%",
                    paddingTop: "30%"
                }}
            >
                <CircularProgress
                    color="secondary"
                    size={100}
                />
            </div>
        )
    }

    return postContainer;
};

export default PostPage;