import React from 'react';
import Layout from "./components/UI/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import HomePage from "./containers/HomePage/HomePage";
import PostPage from "./containers/PostPage/PostPage";
import NewPost from "./containers/NewPost/NewPost";

const App = () => {
    return (
        <Layout>
            <Switch>
                <Route exact path='/' component={HomePage}/>
                <Route path='/posts/:id' component={PostPage}/>
                <Route path='/register' component={Register}/>
                <Route path='/login' component={Login}/>
                <Route path='/addpost' component={NewPost}/>
            </Switch>
        </Layout>
    );
};

export default App;