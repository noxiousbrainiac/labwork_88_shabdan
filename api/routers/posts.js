const express = require('express');
const Post = require('../models/Post');
const multer = require('multer');
const {nanoid} = require('nanoid');
const config = require('../config');
const path = require("path");
const auth = require('../middleware/auth');
const moment = require('moment');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.post('/', auth, upload.single('image'), async (req, res) => {
    try {
        if (!req.body.title) return res.status(400).send({error: 'Invalid data'});

        if (!req.file && !req.body.description) {
            return res.status(400).send({error: "Enter required inputs"});
        }

        const postData = {
            title: req.body.title,
            description: req.body.description,
            datetime: moment().format('MMMM Do YYYY, h:mm:ss a'),
            user_id: req.user._id
        };

        if (req.file) postData.image = 'uploads/' + req.file.filename;

        const post = new Post(postData);
        await post.save();
        res.send(post);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.get('/', async (req, res) => {
    try {
        const posts = await Post.find().populate('user_id', 'username').sort({datetime: -1});
        res.send(posts);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const post = await Post.findById(req.params.id);
        res.send(post);
    } catch (e) {
        res.status(500).send(e);
    }
});

module.exports = router;
