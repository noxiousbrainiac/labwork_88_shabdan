const express = require('express');
const Comment = require('../models/Comment');
const Post = require('../models/Post');
const auth = require('../middleware/auth');
const moment = require("moment");

const router = express.Router();

router.post('/', auth, async (req, res) => {
    try {
        if (!req.body.text) return res.status(400).send({error: "Invalid data"});

        const post = await Post.findOne({_id: req.body.post_id});

        if (!post) return res.status(404).send({error: "No post with following ID"});

        const commentData = {
            text: req.body.text,
            user_id: req.user._id,
            datetime: moment().format('MMMM Do YYYY, h:mm:ss a'),
            post_id: post._id
        }

        const comment = new Comment(commentData);
        await comment.save();
        res.send(comment);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.get('/',async(req, res) => {
   try {
       if (req.query.post) {
           const comments = await Comment.find({post_id: req.query.post})
               .populate('user_id', 'username')
               .sort({datetime: -1});;
           return res.send(comments);
       }
       const comments = await Comment.find();
       res.send(comments);
   } catch (e) {
       res.status(500).send(e);
   }
});

module.exports = router;