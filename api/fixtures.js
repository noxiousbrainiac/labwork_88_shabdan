const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const Post = require('./models/Post');
const User = require('./models/User');
const Comment = require('./models/Comment');
const moment = require("moment");

const run = async () => {
    await mongoose.connect(config.db.url);

    const collections = await  mongoose.connection.db.listCollections().toArray();

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const admin = await User.create({
        username: "admin",
        password: "123",
        token: nanoid(),
    });

    const post = await Post.create({
        title: "Earthquake",
        user_id: admin,
        description: "Today",
        datetime: moment().format('MMMM Do YYYY, h:mm:ss a'),
        image: 'fixtures/earthquake.jpg'
    });

    await Comment.create({
        text: "hehe, cool",
        post_id: post,
        user_id: admin,
        datetime:  moment().format('MMMM Do YYYY, h:mm:ss a')
    });

    await mongoose.connection.close();
};

run().catch(e => console.log(e));