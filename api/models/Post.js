const mongoose = require('mongoose');

const PostSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: function () {
            return !this.image
        }
    },
    image: {
        type: String,
        required: function () {
            return !this.description
        }
    },
    datetime: String,
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: true,
    },
});

const Post = mongoose.model('/Post', PostSchema);

module.exports = Post;